package despachador;

public class Despachador 
{
	static int a = 0;
	static int b = 0;
	
	public static int opc1() {
		a = 1;
		return b;
	}
	
	public static int opc2() {
		b = 2;
		return (3*a);
	}
}

class Hereda extends Despachador implements Runnable
{ 
	int hilo;
	
	public Hereda(int n) {
		hilo = n;
	}

	public void run() {
		
		if ( hilo == 1 )
			System.out.println("Resultado opc1: "+opc1());
		else
			System.out.println("Resultado opc2: "+opc2());
	}
	
	public static void main( String[] args) {
		
		Hereda Objeto1 = new Hereda(1);
		Thread hilo1 = new Thread(Objeto1);
		hilo1.start();
		
		Hereda Objeto2 = new Hereda(2);
		Thread hilo2 = new Thread(Objeto2);
		hilo2.start();
		
	}
}