package mergeSort;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MergeSort extends Thread
{
	int arreglo[];
	
	public int[] getArreglo() {
		return arreglo;
	}
	
	public MergeSort(int aux[]) {
		arreglo = aux;
	}
	
	///Funcion MERGE SORT
	public int[] mergeSort(int desordenado[]) throws InterruptedException
	{
		int mitad, izquierda, derecha, i, j = 0;
		
		//Obtener el tama�o del arreglo a ordenar
		int tamano = desordenado.length;
		
		//Si la longitud es 0 � 1 pr�cticamente ya est� ordenado sino se procede a dividir
		if(tamano > 1) {
			
			mitad = (tamano)/2;
			
			//Si se puede dividir exactamente a la mitad
			if ( (tamano%2) == 0 ) {
				
				izquierda=mitad;
				derecha=mitad;
				
			} else {
				
				izquierda=mitad;
				derecha=mitad+1;
			}
			
			int primerArreglo[] = new int[izquierda];
			int segundoArreglo[] = new  int[derecha];
			
			//Asignar valores
			for(i = 0; i < mitad; i++) {
				primerArreglo[i] = desordenado[i];
			}
			
			for(i=mitad; i<tamano; i++) {				
				segundoArreglo[j] = desordenado[i];
				j++;
			}
			
			//Hilos
			MergeSort ObjIzquierda = new MergeSort(primerArreglo);
			MergeSort ObjDerecha = new MergeSort(segundoArreglo);
			
			ObjIzquierda.start();
			ObjIzquierda.join();
			ObjDerecha.start();

			ObjDerecha.join();
			
			int banderai = 0, banderad = 0;
			
			do {
				
				if(ObjIzquierda.getState() == Thread.State.TERMINATED) {
					banderai = 1;
				}
				
				if(ObjDerecha.getState() == Thread.State.TERMINATED) {
					banderad = 1;
				}
				
			} while( (banderai == 0) || (banderad == 0) );
			
			primerArreglo = ObjIzquierda.getArreglo();
			segundoArreglo = ObjDerecha.getArreglo();
			
			j = 0;
			
			if(primerArreglo[mitad-1]<=segundoArreglo[0]) {
				
				for(i=0; i<mitad; i++) {
					desordenado[i] = primerArreglo[i];
				}
				
				for(i=mitad; i<tamano; i++) {
					desordenado[i] = segundoArreglo[j];
					j++;
				}
				
				System.out.println("Ordenamiento " + this.getId() + "");
				
				for(i = 0; i < tamano; i++) {
					System.out.println("Hilo " + this.getId() + ": "+ desordenado[i] + "");
				}
				
				System.out.println();
				return desordenado;
			
			} else {
				
				System.out.println("Ordenamiento " + this.getId() + "");
				desordenado = merge(primerArreglo, segundoArreglo);
				
				for(i = 0; i < tamano; i++) {
					System.out.println("Hilo "+ this.getId()+": "+desordenado[i] + "");
				}
				
				System.out.println();
				return desordenado;
			}
		
		} else {
			//En este caso, la longitud es 0 o 1, por lo que ya est� ordenado.
			return desordenado;
		}
	}
	
	///Funcion MERGE
	public int[] merge(int arregloIzquierda[], int arregloDerecha[])
	{
		int tamanoResultante = (arregloIzquierda.length + arregloDerecha.length);
		int resultado[] = new int[tamanoResultante];
		int contador = 0, i = 0, j = 0;
		int finalI = 0;
		int tamanoIzquierda = arregloIzquierda.length, tamanoDerecha=arregloDerecha.length;
		
		while( (contador + 1) < tamanoResultante ) {
			
			if(arregloIzquierda[i]<= arregloDerecha[j]) {
				
				resultado[contador] = arregloIzquierda[i];
				contador++;
				i++;
				
			} else {
				
				resultado[contador] = arregloDerecha[j];
				contador++;
				j++;
			}
			
			if(i == tamanoIzquierda) {
				
				finalI=1;
				break;
			}
			
			if(j == tamanoDerecha){
				break;
			}
		}
		
		if (finalI == 1) {
			
			while(contador<tamanoResultante) {
				
				resultado[contador] = arregloDerecha[j];
				contador++;
				j++;
			}
			
		} else {
			
			while(contador<tamanoResultante) {
				
				resultado[contador] = arregloIzquierda[i];
				contador++;
				i++;
			}
		}
		return resultado;
	}
	
	///Funcion para mostrar el arreglo
	public void Mostrar() {
		
		System.out.println("(Arreglo con id " + this.getId() + ")");
		
		for(int i=0; i<arreglo.length; i++) {
			System.out.println("("+this.getId()+") "+arreglo[i] + " | ");
		}
		
		System.out.println();
	}
	
	///Implementacion del m�todo Run
	public void run()
	{
		int desordenado[] = new int[arreglo.length];
		desordenado = arreglo;
		
		try {
			arreglo = mergeSort(desordenado);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public static void main(String[] args) throws NumberFormatException, IOException, InterruptedException {
		
		int i;
		
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		
		System.out.print("\nIngrese el tama�o del arreglo: ");
		int tamanoArreglo = Integer.parseInt(br.readLine());
		
		int limiteMayor = tamanoArreglo*2;
		int Arreglo[] = new int[tamanoArreglo];
		
		for(i = 0; i < tamanoArreglo; i++) {
			Arreglo[i] = (int) Math.floor(Math.random()*limiteMayor);
			double positivo = Math.random();
			if(positivo < 0.5) {
				
				Arreglo[i]=Arreglo[i]*(-1);
			}
		}
		
		System.out.println("Arreglo a ordenar");
		
		for(i=0; i<tamanoArreglo; i++) {
			System.out.print(Arreglo[i] + " | ");
		}
		
		System.out.println("\n");
		
		MergeSort Primerhilo = new MergeSort(Arreglo);
		Primerhilo.start();
		Primerhilo.join();
		
		Arreglo = Primerhilo.getArreglo();
		System.out.println("Arreglo ordenado");
		
		for(i=0; i<tamanoArreglo; i++) {
			System.out.print(Arreglo[i] + " | ");
		}
		System.out.println();
	}
}
